# Canecom iOS tesztfeladat

Készíts egy **UICollectionView controller**-t, ami "**végtelen scrollozás**ra" és **lazy loading**-ra képes.

Az adatokat egy URL-ről töltse le, és minden kéréshez legyen egy offset változó ami ahogy az oldalak közt mozgok, nő illetve csökken.
Az adatok mennyisége egy limit változóban legyen megadható, nehezítés, hogy nem tudhatjuk hány adat van összesen a szerveren.

pl. [http://localhost/items.json?offset=1&limit=10](http://localhost/items.json?offset=1&limit=10)  
pl. [http://localhost/items.json?offset=2&limit=10](http://localhost/items.json?offset=2&limit=10)  
pl. [http://localhost/items.json?offset=3&limit=10](http://localhost/items.json?offset=3&limit=10)

A példa adat álljon a lennt szereplő struktúrából, és a cellán legyen egy kép és egy szöveg mező megjelenítve..
A képek betöltése is lazy legyen. Ne a fő szálon történjen, értelem szerűen nem befolyásolhatja a scrollozás sebességét.

Fontos, hogy az adatforrásba mindig csak az aktuális oldal legyen betöltve, illetve tárolhatja még az előtte és utána levő oldalakat is, de többet nem.
Ahogy mozgok a collectionview-ban, és elérem a lista végét / elejét (vagy előtte 1-2 cellával) akkor töltse be a következő/előző adatforrást ha van.

Legyen opció arra, hogy ha elérem a lista elejét, vagy végét, akkor folytassa-e végtelenítve a scrollozást értelem szerűen a lista végére érve az első oldalt
töltse be, a lista elejére érve pedig visszafele scrollozva az utolsó, majd utolsó előtti, és így tovább oldalak közt tudjon haladni.

Egy kis segítség: [http://adoptioncurve.net/archives/2013/07/building-a-circular-gallery-with-a-uicollectionview/](http://adoptioncurve.net/archives/2013/07/building-a-circular-gallery-with-a-uicollectionview/)

Példa adatforrás (items.json):

```json
[
{
"id" : "item_0001",
"title" : "Item 0001",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0002",
"title" : "Item 0002",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0003",
"title" : "Item 0003",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0004",
"title" : "Item 0004",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0005",
"title" : "Item 0005",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0006",
"title" : "Item 0006",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0007",
"title" : "Item 0007",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0008",
"title" : "Item 0008",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0009",
"title" : "Item 0009",
"images" : "http://loremflickr.com/320/240?random=1"
},
{
"id" : "item_0010",
"title" : "Item 0010",
"images" : "http://loremflickr.com/320/240?random=1"
}
]
```