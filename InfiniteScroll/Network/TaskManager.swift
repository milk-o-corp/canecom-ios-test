//
//  TaskManager.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 03..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}

final class TaskManager<T: Decodable> {

    typealias TaskHandler = (Result<T>) -> Void
    let session: URLSession

    init(session: URLSession = .init(configuration: .default)) {
        self.session = session
    }

    @discardableResult
    func dataTask(with url: URL, completion: @escaping TaskHandler) -> URLSessionDataTask {
        let task = session.dataTask(with: url, completionHandler: {(data, _, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else { return } // TODO: @jocsaszi Improve error handling
            do {
                let decoded = try JSONDecoder().decode(T.self, from: data)

                completion(.success(decoded))
            } catch let err {
                completion(.failure(err))
            }
        })
        task.resume()

        return task
    }
}
