//
//  ItemFetcher.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 03..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import Foundation

struct PageData<T> {
    var page: Int
    var data: T
}

final class ItemFetcher {
    public let limit: Int
    public let isCircular: Bool
    private let baseURL: URL

    private let taskManager = TaskManager<[Item]>()
    private var dataSet = SlidingGate<PageData<[Item]>>(maxSize: 3)
    private var tasks = [Int: URLSessionDataTask]()
    private var isLastPageFound: Bool = false
    private var lastPage: Int = 0
    private let queue = DispatchQueue(label: "ItemFetcherQueue", attributes: .concurrent)

    init(baseURL: URL, limit: Int, isCircular: Bool = false) {
        self.baseURL = baseURL
        self.limit = limit
        self.isCircular = isCircular
    }

    func getNext(_ handler: @escaping (SlidingGate<PageData<[Item]>>) -> ()) {
        let current = dataSet.storage.last?.page ?? 0
        var nextPage = current + 1
        if isLastPageFound && isCircular && nextPage >= lastPage {
            nextPage = 1
        }
        guard tasks[nextPage] == nil else {
            return
        }

        queue.sync { [weak self] in
            self?.get(page: nextPage, direction: .forward, handler: handler)
        }
    }

    func getPrevious(_ handler: @escaping (SlidingGate<PageData<[Item]>>) -> ()) {
        guard let current = dataSet.storage.first?.page else { return }
        var prevPage = current - 1

        if prevPage < 1 {
            guard isCircular else {
                return
            }
            prevPage = lastPage
        }
        guard tasks[prevPage] == nil else {
            return
        }
        queue.sync { [weak self] in
            self?.get(page: prevPage, direction: .backward, handler: handler)
        }
    }

    private func get(page: Int, direction: Direction = .forward, handler: @escaping (SlidingGate<PageData<[Item]>>) -> ()) {
        guard let url = buildURL(for: page, with: limit) else {
            assertionFailure("Invalid URL")
            return
        }

        let task = taskManager.dataTask(with: url) { [weak self] (result) in
            guard let self = self else { return }

            switch result {
            case let .success(items):
                if !items.isEmpty {
                    self.storeLastPage(page)
                    let pageData = PageData(page: page, data: items)
                    if direction == .forward {
                        self.dataSet.append(pageData)
                    } else {
                        self.dataSet.prepend(pageData)
                    }
                    if items.count < self.limit {
                        self.isLastPageFound = true
                    }
                    handler(self.dataSet)
                } else if !self.isLastPageFound {
                    self.isLastPageFound = true
                    if self.isCircular {
                        self.get(page: 1, direction: direction, handler: handler)
                    }
                }

            case let .failure(err):
                // TODO: @jocsaszi Improve error handling
                print(err.localizedDescription)
                handler(self.dataSet)
            }
            self.tasks[page]?.cancel()
            self.tasks.removeValue(forKey: page)
        }
        tasks[page] = task
    }

    private func storeLastPage(_ index: Int) {
        if !isLastPageFound && lastPage < index {
            lastPage = index
        }
    }

    private func buildURL(`for` page: Int, with limit: Int) -> URL? {
        guard page > 0 else { return nil }
        let offset = page - 1
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "limit", value: String(limit)),
                                          URLQueryItem(name: "offset", value: String(offset))]
        var urlComps = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)
        urlComps?.queryItems = queryItems

        return urlComps?.url
    }
}
