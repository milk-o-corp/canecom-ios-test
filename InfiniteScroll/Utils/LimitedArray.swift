//
//  LimitedArray.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 04..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import Foundation

struct SlidingGate<T> {
    private(set) var storage: [T] = []
    public let maxSize: Int

    init(maxSize: Int) {
        self.maxSize = maxSize
        storage.reserveCapacity(maxSize)
    }

    mutating func append(_ item: T) {
        if storage.count == maxSize {
            storage.removeFirst()
        }
        storage.append(item)
    }

    mutating func prepend(_ item: T) {
        if storage.count == maxSize {
            storage.removeLast()
        }
        storage.insert(item, at: 0)
    }
}

extension SlidingGate where T == PageData<[Item]> {
    func getDataSource(circular isCircular: Bool = false) -> [Item] {
        var flattenedDataSource = storage.map { $0.data }.reduce([], +)
        if isCircular {
            makeCircular(&flattenedDataSource)
        }
        return flattenedDataSource
    }

    private func makeCircular(_ dataSource: inout [Item]) {
        if let first = dataSource.first, let last = dataSource.last {
            dataSource.insert(last, at: 0)
            dataSource.append(first)
        }
    }
}
