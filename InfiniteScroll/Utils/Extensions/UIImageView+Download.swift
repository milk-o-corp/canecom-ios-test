//
//  UIImageView+Download.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 04..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import UIKit

extension UIImageView {
    @discardableResult
    func setImage(urlString: String) -> URLSessionDataTask? {
        guard let url = URL(string: urlString) else { return nil }
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error {
                // TODO: @jocsaszi Improve error handling
                print(error.localizedDescription)
                return
            }
            guard let imageData = data else { return }
            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.image = image
            }
        }
        task.resume()

        return task
    }
}
