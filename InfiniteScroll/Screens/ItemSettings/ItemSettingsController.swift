//
//  ItemSettingsController.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 03..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import UIKit

final class ItemSettingsController: UIViewController {

    // tf stands for TextField
    @IBOutlet private weak var tf_url: UITextField!
    @IBOutlet private weak var tf_limit: UITextField!
    // sc stands for SegmentControl
    @IBOutlet private weak var sc_flowType: UISegmentedControl!
    // this prefix-style (with type-abbr) is only applied for ui elements

    @IBAction func showCollection(_ sender: Any) {
        var urlString = "http://localhost:3000/items.json"
        if let text = tf_url.text, !text.isEmpty {
            urlString = text
        }
        guard let url = URL(string: urlString) else {
            showAlert(title: "Warning", message: "Invalid URL\n\(urlString)")
            return
        }
        let isCircular = sc_flowType.selectedSegmentIndex == 1

        var limit = 10
        if let limitText = tf_limit.text, let limitValue = Int(limitText) {
            limit = limitValue
        }
        guard limit > 0 else {
            showAlert(title: "Warning", message: "The limit zero doesn't make sense")
            return
        }
        let vc = ItemCarouselController(itemFetcher: ItemFetcher(baseURL: url, limit: limit, isCircular: isCircular))

        navigationController?.pushViewController(vc, animated: true)
    }
}

private extension UIViewController {
    func showAlert(title: String, message: String, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let done = UIAlertAction(title: "Ok", style: .default) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(done)

        navigationController?.present(alertController, animated: true, completion: completion)
    }
}
