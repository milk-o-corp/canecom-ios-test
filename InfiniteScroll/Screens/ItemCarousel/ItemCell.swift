//
//  ItemCell.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 03..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import UIKit

final class ItemCell: UICollectionViewCell {

    static var identifier: String = "ItemCell"
    private weak var lbl_title: UILabel!
    private weak var ivw_itemImage: UIImageView!
    private var imageLoadingTask: URLSessionDataTask?

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupImageView()
        setupTitleLabel()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        imageLoadingTask?.cancel()
    }

    // MARK: - Configuration

    func configure(with item: Item) {
        lbl_title.text = item.title
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.imageLoadingTask = self?.ivw_itemImage.setImage(urlString: item.images)
        }
    }

    // MARK: - Create views

    private func setupTitleLabel() {
        let textLabel = UILabel(frame: .zero)
        textLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.textAlignment = .center
        textLabel.text = ""
        textLabel.textColor = .black
        textLabel.backgroundColor = .yellow
        textLabel.numberOfLines = 0
        contentView.addSubview(textLabel)
        lbl_title = textLabel
    }

    private func setupImageView() {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        contentView.addSubview(imageView)
        ivw_itemImage = imageView
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: ivw_itemImage.topAnchor),
            contentView.rightAnchor.constraint(equalTo: ivw_itemImage.rightAnchor),
            contentView.bottomAnchor.constraint(equalTo: ivw_itemImage.bottomAnchor),
            contentView.leftAnchor.constraint(equalTo: ivw_itemImage.leftAnchor),

            ivw_itemImage.leadingAnchor.constraint(equalTo: lbl_title.leadingAnchor, constant: -20),
            ivw_itemImage.trailingAnchor.constraint(equalTo: lbl_title.trailingAnchor, constant: 20),
            ivw_itemImage.bottomAnchor.constraint(equalTo: lbl_title.bottomAnchor)
            ])
    }
}
