//
//  ItemCarouselController.swift
//  InfiniteScroll
//
//  Created by Csaszi, Jozsef on 2018. 12. 03..
//  Copyright © 2018. Csaszi, Jozsef. All rights reserved.
//

import UIKit

enum Direction {
    case forward
    case backward
}

final class ItemCarouselController: UIViewController {

    private lazy var collectionView: UICollectionView = makeCollectionView()
    private var lastContentOffset: CGFloat = 0
    private var scrollDirection: Direction = .forward

    private var dataSource = [Item]()
    private let itemFetcher: ItemFetcher
    private var isCircular: Bool {
        return itemFetcher.isCircular
    }

    init(itemFetcher: ItemFetcher) {
        self.itemFetcher = itemFetcher
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "\(isCircular ? "Circular" : "Linear") - Limit: \(itemFetcher.limit)"
        view.backgroundColor = .white
        addCollectionView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        loadInitialData()
    }

    // MARK: - Setup UI
    private func addCollectionView() {
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: collectionView.topAnchor),
            view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor),
            view.safeAreaLayoutGuide.leadingAnchor.constraint(equalTo: collectionView.leadingAnchor),
            view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: collectionView.trailingAnchor)
            ])
    }

    private func makeCollectionView() -> UICollectionView {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ItemCell.self, forCellWithReuseIdentifier: ItemCell.identifier)
        collectionView.alwaysBounceVertical = false
        collectionView.backgroundColor = .white

        return collectionView
    }

    // MARK: - Setup initial Data
    private func loadInitialData() {
        guard dataSource.isEmpty else { return }
        itemFetcher.getNext { [weak self] (dataSet) in
            guard let self = self else { return }
            self.dataSource = dataSet.getDataSource(circular: self.isCircular)

            // TODO: @jocsaszi Handle empty dataSet. A refresh button in the navigationBar can fit well.
            guard !self.dataSource.isEmpty else { return }

            DispatchQueue.main.async {
                self.collectionView.reloadData()
                let row = self.isCircular && self.dataSource.count > 2 ? 1 : 0
                self.collectionView.scrollToItem(at: IndexPath(row: row, section: 0), at: .left, animated: false)
            }
        }
    }
}

extension ItemCarouselController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCell.identifier, for: indexPath) as? ItemCell else {
                                                                fatalError("Dequeue ItemCell failed at \(indexPath)")
        }
        let index = indexPath.item
        let item = dataSource[index]
        cell.configure(with: item)

        return cell
    }
}

extension ItemCarouselController: UICollectionViewDelegate {

    // MARK: - CollectionView delegate
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let index = indexPath.item
        switch scrollDirection {
        case .forward:
            if canLoadNextDataSource(for: index) {
                itemFetcher.getNext(handleDataSet)
            }
        case .backward:
            if canLoadPrevDataSource(for: index) {
                itemFetcher.getPrevious(handleDataSet)
            }
        }
    }

    // MARK: - UI Helpers
    private func canLoadNextDataSource(`for` index: Int) -> Bool {
        return index == getLastItemIndex()
    }

    private func canLoadPrevDataSource(`for` index: Int) -> Bool {
        let realFirstIndex = isCircular ? 3 : 1
        return index == realFirstIndex
    }

    private func getLastItemIndex() -> Int {
        let itemIndex = collectionView.numberOfItems(inSection: 0) - 1
        return isCircular ? itemIndex - 1 : itemIndex
    }

    private func handleDataSet(_ dataSet: SlidingGate<PageData<[Item]>>) {
        let newDataSource = dataSet.getDataSource(circular: isCircular)
        let changeSet = diff(dataSource, newDataSource)
        guard !changeSet.isEmpty else { return }

        let orderedChangeSet = orderedOperation(from: changeSet)
        DispatchQueue.main.async {
            self.applyDiffedOperations(orderedChangeSet) {
                self.dataSource = self.dataSource.merged(with: orderedChangeSet)
            }
        }
    }

    private func applyDiffedOperations(_ operations: [DiffOperation<Item>.Simple], dataSourceUpdate: @escaping () -> ()) {
        UIView.performWithoutAnimation {
            collectionView.performBatchUpdates({
                dataSourceUpdate()
                var deletes = isCircular ? -1 :  0
                var inserts = isCircular ? -2 : 0
                operations.forEach({ (operation) in
                    switch operation {
                    case let .delete(_, atIndex):
                        collectionView.deleteItems(at: [IndexPath(item: atIndex, section: 0)])
                        if let visiblePath = collectionView.indexPathsForVisibleItems.last,
                            scrollDirection == .forward, visiblePath.item > atIndex {
                            deletes += 1
                        }
                    case let .add(_, atIndex):
                        collectionView.insertItems(at: [IndexPath(item: atIndex, section: 0)])
                        inserts += 1
                    case let .update(_, _, atIndex):
                        collectionView.reloadItems(at: [IndexPath(item: atIndex, section: 0)])
                    }
                })

                let currentOffset = collectionView.contentOffset
                let pageWidth = collectionView.frame.width

                if deletes > 0 && self.scrollDirection == .forward {
                    let offset = CGFloat(deletes) * pageWidth
                    collectionView.contentOffset = CGPoint(x: currentOffset.x - offset, y: currentOffset.y)
                } else if inserts > 0 && scrollDirection == .backward {
                    let offset = CGFloat(inserts) * pageWidth
                    collectionView.contentOffset = CGPoint(x: currentOffset.x + offset, y: currentOffset.y)
                }
            })
        }
    }

    // MARK: - ScrollView delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // We can ignore the first time scroll,
        // because it is caused by the call scrollToItemAtIndexPath: in viewWill/DidAppear
        if lastContentOffset == .leastNormalMagnitude {
            lastContentOffset = scrollView.contentOffset.x
            return
        }

        let currentX = scrollView.contentOffset.x
        if lastContentOffset > currentX {
            scrollDirection = .backward
        } else if lastContentOffset < currentX {
            scrollDirection = .forward
        }
        if !isCircular {
            lastContentOffset = currentX
            return
        }

        let currentY = scrollView.contentOffset.y
        let pageWidth = scrollView.frame.size.width
        let offset = pageWidth * CGFloat(dataSource.count - 2)

        // the first page(showing the last item) is visible and user's finger is still scrolling to the right
        if currentX < pageWidth && lastContentOffset > currentX {
            lastContentOffset = currentX + offset
            scrollView.contentOffset = CGPoint(x: lastContentOffset, y: currentY)
        }
        // the last page (showing the first item) is visible and the user's finger is still scrolling to the left
        else if currentX > offset && lastContentOffset < currentX {
            lastContentOffset = currentX - offset
            scrollView.contentOffset = CGPoint(x: lastContentOffset, y: currentY)
        } else {
            lastContentOffset = currentX
        }
    }
}

extension ItemCarouselController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width, height: floor(view.bounds.height * 3 / 4))
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
